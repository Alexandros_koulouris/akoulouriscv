// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAB8a6rmO9pBAJOBIu5exdN1G_NYLtsh_k",
    authDomain: "akoulouriscv.firebaseapp.com",
    databaseURL: "https://akoulouriscv.firebaseio.com",
    projectId: "akoulouriscv",
    storageBucket: "akoulouriscv.appspot.com",
    messagingSenderId: "379564772407"
  }
};
