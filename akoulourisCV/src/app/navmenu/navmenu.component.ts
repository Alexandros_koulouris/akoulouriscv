 import { Component, OnInit,ViewChild,ChangeDetectorRef } from '@angular/core';
// import {Component, trigger, state, style, transition, animate,OnInit} from '@angular/core';
 import { trigger, state, style, transition, animate} from '@angular/animations';
 import {MatSidenav} from '@angular/material/sidenav';
 import {MediaMatcher} from '@angular/cdk/layout';

 import { ElementRef } from '@angular/core';

 

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html', 
  
  // template: `
  //   <p>
  //     navmenu works!
  //   </p>
  // `,
 // styles: []
 styleUrls: ['./navmenu.component.css'],
 animations: [
  trigger('slideInOut', [
    state('in', style({
      transform: 'translate3d(0, 0, 0)'
    })),
    state('out', style({
      transform: 'translate3d(100%, 0, 0)'
    })),
    transition('in => out', animate('400ms ease-in-out')),
    transition('out => in', animate('400ms ease-in-out'))
  ]),
]
})
export class NavmenuComponent implements OnInit {
 
  private typewriter_text: string = "Thank you for your interest\r\nbdgb";
  private typewriter_display: string = "";

  typingCallback(that) {
    let total_length = that.typewriter_text.length;
    let current_length = that.typewriter_display.length;
    if (current_length < total_length) {
      that.typewriter_display += that.typewriter_text[current_length];
    } else {
      that.typewriter_display = "";
    }
    setTimeout(that.typingCallback, 200, that);
  }

  // show = false;

  // constructor() { }

  // get stateName() {
  //   return this.show ? 'show' : 'hide'
  // }


  // toggle() {
  //   this.show = !this.show;
  // }
  // constructor(private myElement: ElementRef) {
  // }
  // @ViewChild("#target") trackerTable: ElementRef;
  scrollexp() {

    document.getElementById("#exp").scrollIntoView({behavior: "smooth"});
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
    //var   nametable= ((document.getElementById("#addcoment") as HTMLInputElement).value);
    
  }

  over(){
    //document.getElementById("#experiance").style.backgroundColor= "red";
    console.log("Mouseover called");
  }

  scrolledu() {
    //this.trackerTable.nativeElement.scrollTo(0,0);
    //let el = this.myElement.nativeElement.querySelector('target');
    //el.scrollIntoView({behavior: "smooth"})
   /// window.scrollTo(0,400)
   console.log(document.getElementById("#target"));
  
    document.getElementById("#target").scrollIntoView({behavior: "smooth", block: "center",inline :"center"});
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
   //window.scrollTo(element.yPosition)
   //element.scrollIntoView(true);
    //window.scrollTo(0,400)
    }

  menuState:string = 'out';

  
onResize(event) {
  console.log(window.screen.width)
}
 
  toggleMenu() {
    
    // 1-line if statement that toggles the value:
    //window.scrollBy(0, 300);
   //window.scrollTo(0, angular.element('put here your element').offsetTop);   980
    this.menuState = this.menuState === 'out' ? 'in' : 'out'; 
    console.log(window.screen.width)
     if(window.screen.width<980){
    if(this.menuState === 'in')
    {
      console.log(screen.width)
      document.getElementById("addcom").style.display = "none";
      //console.log(document.getElementById("navmenu"));
    }
    else{
      document.getElementById("addcom").style.display = "block";
    }

  }
  }
  // constructor(public name: string, public state = 'inactive') { }

  // toggleState() {
  //   this.state = this.state === 'active' ? 'inactive' : 'active';
  // }
 
  // mobileQuery: MediaQueryList;

  // fillerNav = Array(50).fill(0).map((_, i) => `Nav Item ${i + 1}`);

  // fillerContent = Array(50).fill(0).map(() =>
  //     `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
  //      labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
  //      laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
  //      voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
  //      cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);

  // private _mobileQueryListener: () => void;

  // constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
  //   this.mobileQuery = media.matchMedia('(max-width: 600px)');
  //   this._mobileQueryListener = () => changeDetectorRef.detectChanges();
  //   this.mobileQuery.addListener(this._mobileQueryListener);
  // }

  // ngOnDestroy(): void {
  //   this.mobileQuery.removeListener(this._mobileQueryListener);
  // }

  // shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

  ngOnInit() {
    this.typingCallback(this);
  }

}
