import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import {NoopAnimationsModule} from '@angular/platform-browser/animations'
import {MatDividerModule,MatCardModule, MatButtonModule, MatCheckboxModule,MatIconModule,MatSidenavModule ,
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { AppComponent } from './app.component';
import { RouterModule,Routes} from '@angular/router';
// import{HomeComponent} from './home/home.component';
import { Router } from '@angular/router';
import { MainComponent } from './main/main.component';
import { NavmenuComponent } from './navmenu/navmenu.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, transition, animate} from '@angular/animations';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

import{AngularFireModule} from 'angularfire2';
import{AngularFireDatabaseModule} from 'angularfire2/database';
// import { environment } from '../environments/environment.prod';
import { environment } from './../environments/environment';


const appRoutes: Routes = [
  { path: 'app-main', component: MainComponent },
  { path: '**', redirectTo: 'app-main' },
  //{ path: 'app-navmenu', component: NavmenuComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavmenuComponent
    
  ],
  imports: [
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,

    MatSidenavModule,
    MatIconModule ,
    NgbModule.forRoot(),
    MatDividerModule,
    MatCardModule,
    MatCheckboxModule,
    MatButtonModule,
    //NoopAnimationsModule,
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
     
    )

  //   RouterModule.forRoot([
  //     { path: '', redirectTo: '/home', pathMatch: 'full' },
  //     { path: '/home', component: HomeComponent },
  //     { path: '**', redirectTo: 'home' }
  // ] )
  ],
  exports:[RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {  
}
