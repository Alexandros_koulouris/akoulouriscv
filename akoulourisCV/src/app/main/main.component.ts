import { Component, OnInit } from '@angular/core';
import{AngularFireDatabase} from 'angularfire2/database';

declare var libraryVar: any;

@Component({
  selector: 'app-main',
  // template: `
  //   <p>
  //     main works!
  //   </p>
  // `,
   templateUrl: './main.component.html',
    styleUrls: ['./main.component.css']
    // styles: ['./main.component.css']
})
export class MainComponent implements OnInit {
  
  scrolltop() {

    
    window.scroll({
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

  

  // constructor() { }

  ngOnInit() {
    
  }

  db:AngularFireDatabase;
  public items;
  public name;
  
  
 
  
  
  comments:any[];
  constructor(db:AngularFireDatabase){
    //this.comments = db.list('/comments').valueChanges();
    //db.list('/comments', ref => ref.orderByKey().limitToLast(1)).valueChanges()
    //db.list('/items', ref => ref.orderByChild('size').equalTo('large'))
    db.list('/comments',ref => ref.orderByChild('timestamp')).valueChanges()
      .subscribe(comments=>{
        this.comments =comments;
       // var   nametable= ((document.getElementById("#price") as HTMLInputElement).value);
       // console.log(nametable);
         this.items = db.list('/comments');
      })

  }

  public AddComment(sName: string, dPrice: number): void {
    const date = Date.now();
    
    let newName =  sName;
    let newcomment =dPrice;
    if(sName){
    this.items.push({
      name:newName,
      comment: dPrice,
     //createdDate: (new Date()).getTime()
     createdDate: new Date().toLocaleString(),
     timestamp :0-date,   
  });
}
else{
  alert('Enter task');
}

   
  }


//   const newObjRef = firebase.database()
//   .ref('usernames')
//   .child(userId)
//   .child('mylibrary/')
//   .push();
// newObjRef.set({
//   type: terrainType,
//   name: terrainName
// });



}
