import { Component } from '@angular/core';
import { trigger, state, style, transition, animate} from '@angular/animations';
import{AngularFireDatabase} from 'angularfire2/database';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // menuState:string = 'in';
 
  // toggleMenu() {
    
  //   this.menuState = this.menuState === 'out' ? 'in' : 'out';
  // }
  title = 'app';
  db:AngularFireDatabase;
  public items;
  
  
  comments:any[];
  constructor(db:AngularFireDatabase){
    //this.comments = db.list('/comments').valueChanges();
    db.list('/comments').valueChanges()
      .subscribe(comments=>{
        this.comments =comments;
        console.log(this.comments);
        // const items = db.list('comments');
        // let newBook =  'alex';
        // items.push(newBook);
         this.items = db.list('/comments');

      })
  }

  public AddBook(sName: string, dPrice: number): void {
    let newBook =  sName;
   
    this.items.push(newBook);
  }


}
